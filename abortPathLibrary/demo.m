clear
close all
vX = 50;
vZ = 0;
roll = 0 ;
rollMaxAngle = deg2rad(25);
rollRate = deg2rad(15);
decceleration = 0.57;
maxDeccZ = 0;
deccZResolution = 0.5;
rollResolution = deg2rad(0.33334);
vMin = 0;
reactionTime = 0.1;

minState = [25,0,0];
maxState = [26,2,deg2rad(1)];
resolutionState = [10,5,deg2rad(5)];

%[X,Y,Z,Heading,Roll,time] = generateChangingCurvatureTrajectory2(vX,vZ,roll,rollMaxAngle,rollRate,decceleration,deccZ,vMin,reactionTime);
%[ X,Y,Z,Heading,Roll,T ] = generateAllPathsForState(vX,vZ,roll,rollResolution,rollMaxAngle,rollRate,decceleration,maxDeccZ,deccZResolution,vMin,reactionTime);
[globalPathList,globalPathIds,stateMatrix] = generateAllPaths(minState,maxState,resolutionState,rollResolution,rollMaxAngle,rollRate,decceleration,maxDeccZ,deccZResolution,vMin,reactionTime);

%% check
state = [25,0,0];
stateIds = value2Id(state,minState,resolutionState);

ids = globalPathIds(stateMatrix(stateIds(1),stateIds(2),stateIds(3)).pathIds(1),:);
plot3(globalPathList(ids(1):ids(2),2),globalPathList(ids(1):ids(2),3),globalPathList(ids(1):ids(2),4),'.')
xlabel('X')
ylabel('Y')
zlabel('Z')
axis equal

%% Initializing a map
addpath ../maps
global mapResolution
mapResolution = 68;
%% assembling pathMap
maxMapX = max(globalPathList(:,2));
maxMapY = max(globalPathList(:,3));
minMapX = min(globalPathList(:,2))-34;
minMapY = min(globalPathList(:,3))-20;
mapSize = coord2index(maxMapX,maxMapY,mapResolution,minMapX,minMapY);
[ pathMap ] = fillupDataStructure(globalPathList,globalPathIds,mapResolution,minMapX,minMapY,maxMapX,maxMapY,maxState,minState,resolutionState);

%% building a map
map = getMapfromImage('../maps/simpleMap.jpg');

%% querying a velocity and finding a velocity limit
queryState = [25,0,0];
pose = [0,-150,0];
displayMapVelocityPaths(pose,map,queryState,stateMatrix,globalPathList,globalPathIds,minState,resolutionState,minMapX,minMapY,mapResolution);
validPaths = queryVelocityFeasability2(pose,map,stateMatrix,queryState,minState,resolutionState,minMapX,minMapY,mapResolution,globalPathList,globalPathIds);
displayPathsByIdsonMap(pose,globalPathList,globalPathIds,validPaths,mapResolution,minMapX,minMapY,'g');
% make a list of paths with velocity
% [ID] = coord2index(x,y,mapResolution,minMapX,minMapY);
% [x,y] = index2coord(ID,mapResolution,minMapX,minMapY);


%% selecting a trajectory set
addpath optimizedPathSet
close all
occMap.data = 0.3305*ones(size(pathMap));
occMap.min = [minMapX minMapY];
occMap.max = [maxMapX maxMapY];
occMap.resolution = mapResolution;

numPaths = 7;

[ optimizedSet ] = greedyOptimization( stateMatrix(1,1,1).pathIds,globalPathList,globalPathIds,pathMap,numPaths,occMap );
