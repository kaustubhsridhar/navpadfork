function [ indices ] = nonAliasedCoord2Index( coords,occMap )
%NONALIASEDCOORD2INDEX Summary of this function goes here
%   Detailed explanation goes here
indices = coord2indexNotRound(coords(:,1),coords(:,2),occMap.resolution,0,0);
indices(:,1) = indices(:,1) - occMap.min(1)/occMap.resolution;
indices(:,2) = indices(:,2) - occMap.min(2)/occMap.resolution;

distance = indices-floor(indices);
indices(distance(:,1)>0.82,:)=0;
indices(distance(:,1)<0.18,:)=0;

indices(distance(:,2)>0.82,:)=0;
indices(distance(:,2)<0.18,:)=0;

indices(indices(:,1)==0,:)=[];
indices(indices(:,2)==0,:)=[];
indices = floor(indices);
end

