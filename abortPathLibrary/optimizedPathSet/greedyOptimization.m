function [ optimizedSet ] = greedyOptimization( setToReduce,pathList,pathIds,pathMap,numPaths,occMap )
%GREEDYOPTIMIZATION Summary of this function goes here
%   Detailed explanation goes here
handleSet = struct();
handleSet.selectedPathsetFig = figure;
handleSet.coloredPathsetFig = figure;
handleSet.contributionPathsetFig = figure;
handleSet.probabilityPathsetFig = figure;
optimizedSet=[];
setProbability = 0;
c = ['b','c','g','y','r','k'];
benefitProbability = struct();

nonAliasedIndices = struct();
for i=1:size(pathIds,1)
    pInd = pathIds(i,:);
    nonAliasedIndices(i).indices = nonAliasedCoord2Index( pathList(pInd(1):pInd(2),2:3),occMap);
    nonAliasedIndices(i).indices = unique(sub2ind(size(occMap.data), nonAliasedIndices(i).indices(:,1), nonAliasedIndices(i).indices(:,2))); 
end

while( size(optimizedSet,1)<numPaths)
        candidatesLeft = setdiff(setToReduce,optimizedSet);
        candidatesScore = zeros(size(candidatesLeft));
        bestCandidate = [];
        bestProbability = 0;
        for i=1:size(candidatesLeft,1)
            setIds = [optimizedSet;candidatesLeft(i)];
            probability= probabilitySafeTrajectorySet( setIds,pathList,pathIds,nonAliasedIndices,pathMap,occMap );
            candidatesScore(i) = probability;
            if(probability>bestProbability)
                bestCandidate = candidatesLeft(i);
                bestProbability = probability;
            end
        end
        
        candidatesScore = smooth(candidatesScore,3);
        [bestProbability,bestCandidate] = max(candidatesScore);
        bestCandidate = candidatesLeft(bestCandidate);
        
        
        bInd = size(optimizedSet,1)+1;
        set(0,'CurrentFigure',handleSet.selectedPathsetFig)
        clf
        hold on
        displayPathsById(optimizedSet,pathList,pathIds,'k')
        set(0,'CurrentFigure',handleSet.selectedPathsetFig)
        hold on
        displayPathsById(bestCandidate,pathList,pathIds,'g')
        grid on
        xlabel('X(m)'); ylabel('Y(m)');
        
        axis([occMap.min(1),occMap.max(1),occMap.min(2),occMap.max(2)])
        axis equal
        saveas(handleSet.selectedPathsetFig,strcat('figs/pathsSet',num2str(bInd)),'fig')
        generate_sized_figure(strcat('figs/pathsSet',num2str(bInd)),5,5,'image')
        
        set(0,'CurrentFigure',handleSet.coloredPathsetFig)
        clf        
        displayPathsByIdsbyValue(candidatesLeft,pathList,pathIds,candidatesScore);
        grid on        
        xlabel('X(m)'); ylabel('Y(m)');        
        axis([occMap.min(1),occMap.max(1),occMap.min(2),occMap.max(2)])
        axis equal
        saveas(handleSet.coloredPathsetFig,strcat('figs/contributionColored',num2str(bInd)),'fig')
        %generate_sized_figure(strcat('figs/contributionColored',num2str(bInd)),5,5,'pdf')
        
        
        optimizedSet=[optimizedSet;bestCandidate];        
        benefitProbability(bInd).data = candidatesScore - setProbability(end);
        setProbability(size(optimizedSet,1)) = bestProbability;
        
        set(0,'CurrentFigure',handleSet.contributionPathsetFig)
        hold on
        benefitProbability(bInd).data(benefitProbability(bInd).data<0) = 0.000001*rand(1); % hack, have to put it because of inclusion exclusion approx
                    
        for i=1:bInd-1
            plot(benefitProbability(i).data,'k','LineWidth',2)
            hold on
        end
        ldata = benefitProbability(bInd).data;
        plot(ldata,'g','LineWidth',2)
        xlabel('Right to Left'); ylabel('Probability')
        axis([0,169,0,0.1])
        grid on
        saveas(handleSet.contributionPathsetFig,strcat('figs/contributionNum',num2str(bInd)),'fig')
        generate_sized_figure(strcat('figs/contributionNum',num2str(bInd)),5,3,'image')
        
        
        set(0,'CurrentFigure',handleSet.probabilityPathsetFig)
        plotSetProbability = setProbability;
        if(size(plotSetProbability,1)>1)
            if(plotSetProbability(end)<plotSetProbability(end-1))
                plotSetProbability(end)=plotSetProbability(end-1) + 0.00001*rand(1);% hack, have to put it because of inclusion exclusion approx
            end
        end
        hold on
        plot(plotSetProbability,'-o','LineWidth',2,'MarkerSize',10)
        xlabel('Iterations'); ylabel('Probability')
        grid on
        axis([0,7,0,0.5])
        saveas(handleSet.probabilityPathsetFig,strcat('figs/pSafe',num2str(bInd)),'fig')
        generate_sized_figure(strcat('figs/pSafe',num2str(bInd)),5,3,'image')      
        pause
end

end
