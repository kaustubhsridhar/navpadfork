function [ safeVelocity ] = queryMaxSafeVelocity( map,velocityMap,pathMap,vMin,vResolution,globalPath,globalVel)
%QUERYMAXSAFEVELOCITY Summary of this function goes here
%   Detailed explanation goes here


for i=size(globalVel,1):-1:1
validPaths = queryVelocityFeasability(map,velocityMap,pathMap,globalVel(i),vMin,vResolution);
ynegative =0;
ypositive = 0;
    for j=1:size(validPaths,1)
        pathxy = globalPath(validPaths(j,1):validPaths(j,2),1:2);
        if(size(find(pathxy(:,2)<0),1)>1)
            ynegative  =1;
        end
        if(size(find(pathxy(:,2)>0),1)>1)
            ypositive  =1;
        end
    end
    if ynegative>0 && ypositive>0
        safeVelocity = globalVel(i);
        return;
    end
end
    safeVelocity = 0;
end
