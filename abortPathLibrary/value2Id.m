function [ vId ] = value2Id(v,vMin,vResolution)
    vId = floor(bsxfun(@rdivide,bsxfun(@minus,v,vMin),vResolution)) + 1;
end



