function [state, history] = apply_controller_and_dynamics(history, chosen_path, planner_params)
%FORWARD_SIM_TRAJECTORY Summary of this function goes here
%   Detailed explanation goes here

% Update state and history
state = struct(chosen_path);
history_increment = struct(chosen_path);
for field = fieldnames(state)'
    indices = min(planner_params.ind,max(size(chosen_path.(field{1}))));
    state.(field{1}) = chosen_path.(field{1})(indices);
    history_increment.(field{1}) = chosen_path.(field{1})(1:indices);
end

history = [history history_increment];
end