function learner_params = update_log_reg( feature_vec, collision_label, learner_params)
%UPDATE_LOG_REG Summary of this function goes here
%   Detailed explanation goes here

magnitude = Inf;
iter = 1;
while (magnitude > 1 && iter <1000)
    inc = transpose(learner_params.learning_rate*sum(feature_vec.*repmat(collision_label.*(1./(1 + exp(collision_label.*(learner_params.weight*feature_vec)))), [size(feature_vec,1) 1]), 2));
    learner_params.weight = learner_params.weight + inc;
    par = collision_label.*(learner_params.weight*feature_vec);
    magnitude = sum(par < 0)
    if (max(abs(inc)) < 1e-6)
        magnitude = 0;
    end
    iter = iter + 1;
end

end

