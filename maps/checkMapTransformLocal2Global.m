function checkMapTransformLocal2Global(minX,minY,maxX,maxY,resolution,localMap,translation,rotationAngle,bigMapResolution,bigMapMinX,bigMapMinY,bigMap)
%CHECKMAPTRANSFORMGLOBAL2LOCAL Summary of this function goes here
%   Detailed explanation goes here
%% extract globalMap
globalMap = findLocal2GlobalTransformedMap(minX,minY,maxX,maxY,resolution,localMap,translation,rotationAngle,bigMapResolution,bigMapMinX,bigMapMinY,bigMap);

%% draw GlobalMap and local Map rectangle and origin
localExtrema = [minX,minY;minX,maxY;maxX,maxY;maxX,minY;minX,minY];
localBot = [0,0];
globalBot = transformPoint2Global(localBot,rotationAngle,translation);
globalExtrema = transformPoint2Global(localExtrema,rotationAngle,translation);
globalBot = coord2index(globalBot(:,1),globalBot(:,2),resolution,bigMapMinX,bigMapMinY);
globalExtrema = coord2index(globalExtrema(:,1),globalExtrema(:,2),resolution,bigMapMinX,bigMapMinY);
translationID = coord2index(translation(:,1),translation(:,2),resolution,bigMapMinX,bigMapMinY);

figure
imshow(bigMap)
hold on
plot(globalExtrema(:,2),globalExtrema(:,1),'r');
plot(globalBot(2),globalBot(1),'ro','MarkerSize',5);
plot(translationID(2),translationID(1),'go');
%% draw localMap in a seperate window
figure
imshow(localMap)
%% drawing resultantMap
figure
imshow(globalMap)

end

